## Bower avec Netbeans 8.2 (Windows)

### 1. Installer Bower

* Préalable
    
    * Installer [Node.js](https://nodejs.org) (utiliser la version Current)
        
        * Node.js est installé par défaut dans "C:\Program Files\nodejs\"
        * Le gestionnaire de paquet Npm est ajouté à la variable d'environnement Windows "Path" (C:\Users\user\AppData\Roaming\npm)

    * Tester Node (cmd.exe)
        
        ```
        node -v
        ```

        => doit retourner la version de Node
        
    * Tester Npm (cmd.exe)
        
        ```
        npm -v
        ```

        => doit retourner la version de Npm
    
    * Installer [Git](https://git-scm.com/)
    
        Options d'installation :
        
        * Path environment => "Use Git from the Windows Command Prompt";
        * Line endings conversions => "Checkout Windows-style, commit Unix-style line endings";
        * Terminal emulator with Git bash => "Use MinTTY";
        * Pour le reste, laisser par défaut.
        
    * Tester Git (cmd.exe)
        
        ```
        git --version
        ```

        => doit retourner la version de Git
        
    * Redémarrer Windows
    
* Installation globale (système) de [Bower](https://bower.io/)
    
    * Exécuter la commande (cmd.exe)
        
        ```
        npm install -g bower
        ```

    * Tester Bower (cmd.exe)
        
        ```
        gulp -v
        ```

        => doit retourner la version de Bower
        
### 2. Parametrer Bower dans NETBEANS

* Accéder à la fenêtre des options Bower (Menu: Outils>Options, Rubrique : HTML/JS, Onglet: Bower)

    ![Netbeans node options](./img/01_netbeans_bower_options.JPG)

* Renseigner le path de Bower (browse ou search)
* Valider

### 3. Initialisation du projet Netbeans

* Création du fichier des dépendances "bower.json" :

    Exécuter la commande (cmd.exe) à la racine du projet
        
        bower init
    

* Création du fichier "bowerrc" :

    Contenu de base :

        {
            "directory": "public_html/bower_components"
        }

### 3. Utilisation de Bower dans un projet

Exécuter les commandes (cmd.exe) à la racine du projet.

* Installer un package :

        bower install <PACKAGE> --save-dev

* Supprimer un package :

        bower uninstall <PACKAGE> --save-dev

* Lister les packages installés (+ mises à jour dispo) :

        bower list

* Rechercher un package :

        bower search <NAME>

* Afficher les infos d'un package :

        bower info <PACKAGE>
        bower info <PACKAGE>#version

* Mettre à jour un package :

        bower update <PACKAGE> --save-dev

* Mettre à jour tous les packages :

        bower update --save-dev
